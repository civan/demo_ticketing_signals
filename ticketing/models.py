from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail

# Create your models here.

'''
Definicion de un modelo basico de Tickets
'''
class Ticket(models.Model):
    ESTADO = (
        ('1', 'ABIERTO'),
        ('2', 'ASIGNADO'),
        ('3', 'CERRADO'),
    )

    titulo = models.CharField(max_length=250)
    descripcion = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modificacion = models.DateTimeField(auto_now=True)
    usuario_creado = models.ForeignKey(User, related_name='usuario_creado')
    usuario_asignado = models.ForeignKey(User, related_name='usuario_asignado', blank=True, null=True)
    estado = models.CharField(
        max_length=1,
        choices=ESTADO,
        default='1',
    )

    def __str__(self):
        return self.titulo


@receiver(post_save, sender=Ticket)
def enviar_correo(sender, instance, created, **kwargs):
    if created:
        send_mail(
            'Notificacion creada',
            'La notificacion "%s" ha sido registrada con exito' % instance.titulo,
            'root@inani.me',
            ['hola@inani.me'],
            fail_silently=False,
        )

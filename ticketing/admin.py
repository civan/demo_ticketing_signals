from django.contrib import admin

# Register your models here.

from .models import Ticket
from .forms import TicketForm

class TicketAdmin(admin.ModelAdmin):
    #listados de usuarios que estaran activos para listados, filtros, busquedas, orden inicial
    list_display = ['titulo', 'descripcion', 'fecha_creacion', 'fecha_modificacion', 'usuario_creado', 'usuario_asignado', 'estado', ]
    list_filter = ['titulo', 'descripcion', 'fecha_creacion', 'fecha_modificacion', 'usuario_creado', 'usuario_asignado', 'estado', ]
    search_fields = ['titulo', 'descripcion', 'fecha_creacion', 'fecha_modificacion', 'usuario_creado', 'usuario_asignado', 'estado', ]
    ordering = ['fecha_creacion', 'fecha_modificacion', ]
    form = TicketForm

    '''
    modificacion de la consulta en base al tipo de usuario que la realiza, para
    poder filtrar tipos tickets por creador o asignado.
    '''
    def get_queryset(self, request):
        qs = super(TicketAdmin, self).get_queryset(request)

        if request.user.groups.filter(name='tecnicos').exists():
            qs = qs.filter(usuario_asignado=request.user)

        if request.user.groups.filter(name='usuarios').exists():
            qs = qs.filter(usuario_creado=request.user)

        return qs

    '''
    Funcion que carga el formulario inicial tipo TicketForm para nuestro modelo
    Ticket. En base al tipo de usuario, ocultara ciertos campos que solo
    pueden editar los superadmin al resto de usuarios.
    '''
    def get_form(self, request, obj=None, **kwargs):
        if not request.user.is_superuser:
            if not self.exclude:
                self.exclude = ()
            self.exclude += ('usuario_creado', 'usuario_asignado', 'estado', )

        return super(TicketAdmin, self).get_form(request, obj, **kwargs)

    '''
    Funcion para modificar el la funcion de salvado de usuario automatico desde
    el formulario. Este es un buen sitio para anadir datos calculados automatica
    mente al anadir informacion y tener acceso al objeto request para poder
    acceder a la informacion del usuario que la realiza. Cosa que por ejemplo
    con las senales pre/post de guardado de instancias en la base de datos
    no se podria conseguir.
    '''
    def save_model(self, request, obj, form, change):
        super(TicketAdmin, self).save_model(request, obj, form, change)
        if not obj.usuario_creado:
            obj.usuario_creado = request.user
        if obj.usuario_asignado and obj.estado == '1':
            obj.estado = '2'
        obj.save()

admin.site.register(Ticket, TicketAdmin)

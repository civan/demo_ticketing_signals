from django import forms
from .models import Ticket

'''
Definicion de nuestro formulario de Ticket generico para todos los usuarios.
Luego en la gestion del panel del admin para el modelo Ticket, haremos un
control de usuarios y una modificacion al vuelo de los campos que se excluyen
segun los permisos
'''
class TicketForm(forms.ModelForm):

    class Meta:
        fields = ['titulo', 'descripcion', 'usuario_creado', 'usuario_asignado', 'estado', ]
        model = Ticket
